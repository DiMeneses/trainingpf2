## Diogo's guide on GitLab's contribuition flow 
---

### 2 min -	Lecture: Learn about git / Why git is important


**Q:** O que é Git? Porquê que é importante?  
**R:** Git é o programa de controle de versionamento mais usado!  
Estes sistemas de versionamento têm como objetivo facilitar a colaboração de programadores no desenvolvimento de código fonte.  
Portanto, na nossa área Git é fulcral.

**Q:** Quem o inventou e porquê?  
**R:** Em 2005, os programadores de Linux encontraram-se sem nenhuma alternativa grátis e adequada deste tipo de sistema e assim Linus Torvalds, fundador de Linux, desapareceu durante um fim de semana para mais tarde emergir com um novo sistema de versionamento.  
Como Linus alega ser um "bastardo egoísta", inspira-se em si próprio para batizar os seus projetos e assim escolheu o nome Git, um insulto da gíria Britânica para pessoas "desagradáveis e incompetentes".  

**Q:** Como funciona?  
**R:** O conceito base do sistema é relativamente simples: cada repositório git tem um histórico de mudanças no conteúdo entre snapshots. Podem haver vários ramos/branches paralelos de históricos e há um algoritmo que permite avançar ou recuar entre esses diferentes estados.

**Q:** Qual a diferença entre Git e GitHub/GitLab?  
**R:** O GitHub/GitLab são serviços onde podes guardar o teu repositório Git para colaboração online. 


![](gitbranch.png)

## 5 min - Demonstration/Active learning phase

### 1 min -	Criar issue
### 1 min -	Criar merge request
### 2 min - Comandos mais importantes

Ver branches locais:
```
git branch -r
```
Como vemos, não está cá o branch correspondente ao nosso merge request.  
Falta atualizar o repositório local com o GitLab:
```
git pull
```

Se correr o primeiro comando já deve aparecer um novo branch.  
Para entrar nele basta:
```
git checkout <branch-name>
```

Agora estámos no sítio correto para fazer as mudanças necessárias!  
Depois de as fazer podemos ver o estado do Git com:
```
git status
```

![](file-lifecycle.png)

Para registar as mudanças é necessário correr:

```
git add <ficheiro(s) com mudanças>
```
Ou mais habitualmente, regista-se todas as mudanças com:
```
git add .
```
Depois criamos um snapshot, com uma mensagem descritiva:
```
git commit -m "Resolvi o problema X"
```
Finalmente, dámos upload das mudanças para o GitLab:
```
git push
```
### 1 min -	Merge Branch

Depois de se dar merge no GitLab, pode-se também apagar o branch local:

```
git branch --list
git branch -D <branch-name>
```

Este workflow e comandos são tudo que eu preciso de saber no meu trabalho diário com Git.

### 1 min -	Questões?

Obrigado!